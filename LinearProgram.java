/*********************************************
* Takes linear programs of the form
* maximise     c.x
* subject to   Ax <= b and x >= 0
* assuming     b >= 0
*********************************************/

public class LinearProgram {
	
	private double[][] coefficientMatrix;
	private double[] constraintVector;
	private double[] costVector;

	private double[][] tableau;
	private int numberOfVariables;
	private int numberOfConstraints;
	private int[] basisIndices;
	
	public static final boolean PRINT_TABLEAU = true;
	public static final boolean SUPRESS_OUTPUT = false;
	public boolean print;
	
	// modes of operation to decide basis entering policy
	public static final int FIRST_POSITIVE = 0;
	public static final int MOST_POSITIVE = 1;
	public static final int RANDOM = 2;
	private final int mode;
	
	private final static double EPSILON = Math.pow(10, -9);
	
	public LinearProgram(double[][] coefficientMatrix, double[] constraintVector, double[] costVector, int mode, boolean print) {
		this.coefficientMatrix = coefficientMatrix;
		this.constraintVector = constraintVector;
		this.costVector = costVector;
		numberOfVariables = costVector.length;
		numberOfConstraints = constraintVector.length;
		tableau = new double[numberOfConstraints + 1][numberOfVariables + numberOfConstraints + 1];
		basisIndices = new int[numberOfConstraints];
		this.mode = mode;
		this.print = print;
		// write coefficient matrix in tableau
		for (int i = 0; i < numberOfConstraints; i++) {
			for (int j = 0; j < numberOfVariables; j++) {
				tableau[i][j] = coefficientMatrix[i][j];
			}
		}
		// write identity matrix under columns corresponding to slack variables
		for (int i = 0; i < numberOfConstraints; i++) {
			tableau[i][numberOfVariables + i] = 1;
    	}
		// write objective function coefficients below coefficient matrix
		for (int j = 0; j < numberOfVariables; j++) {
			tableau[numberOfConstraints][j] = costVector[j];
		}
		// write constraint vector in rightmost column
		for (int i = 0; i < numberOfConstraints; i++) {
			tableau[i][numberOfVariables + numberOfConstraints] = constraintVector[i];
		}
		// initially slack variables populate the basis
		for (int i = 0; i < numberOfConstraints; i++) {
			basisIndices[i] = numberOfVariables + i;
		}
	}
	
	private boolean isIndexInBasis(int index) {
		for (int i = 0; i < basisIndices.length; i++) {
			if (index == basisIndices[i]) {
				return true;
			}
		}
		return false;
	}
	
	public void print() {
		int rows = numberOfConstraints  + 1;
		int columns = numberOfVariables + numberOfConstraints + 1;
		for (int j = 0; j < columns; j++) {
			System.out.printf("\t ------");
		}
		System.out.println();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.printf("\t%.3f", tableau[i][j]);
			}
			System.out.println();
		}
		for (int j = 0; j < columns; j++) {
			System.out.printf("\t ------");
		}
		System.out.println();
	}
	
	private int getColumnIndexOfPivot() {
		if (mode == FIRST_POSITIVE) {
			for (int j = 0; j < numberOfVariables + numberOfConstraints; j++) {
	            if(tableau[numberOfConstraints][j] > 0) {
	                return j;
	            }
	        }
		} else if (mode == MOST_POSITIVE) {
			int index = 0;
			for (int j = 1; j < numberOfVariables + numberOfConstraints; j++) {
                if (tableau[numberOfConstraints][j] > tableau[numberOfConstraints][index]) {
                    index = j;
                }
            }
            if (tableau[numberOfConstraints][index] > 0) {
                return index;
            }
		} else if (mode == RANDOM) {
			int index = (int) (Math.random() * (numberOfVariables + numberOfConstraints));
            while (isIndexInBasis(index)) {
                index = (int) (Math.random() * (numberOfVariables + numberOfConstraints));
            }
            boolean positiveCostRemaining = false;
            for (int j = 0; j < numberOfVariables + numberOfConstraints; j++) {
            	if (tableau[numberOfConstraints][j] > 0) {
            		positiveCostRemaining = true;
            		break;
            	}
            }
            if (positiveCostRemaining) {
            	return index;
            }
		}
        return -1;
    }
	
	private int getRowIndexOfPivot(int index) {
		int row = -1;
		for (int i = 0; i < numberOfConstraints; i++) {
			if (tableau[i][index] <= 0) {
				continue;
			} else if (row == -1) {
				row = i;
			} else if ((tableau[i][numberOfVariables + numberOfConstraints] / tableau[i][index]) < (tableau[row][numberOfVariables + numberOfConstraints] / tableau[row][index])) {
				row = i;
			}
		}
		return row;
	}
	
	private void pivotOperation(int row, int column) {
		// divide pivot row by pivot
		double pivot = tableau[row][column];
		for (int j = 0; j < numberOfVariables + numberOfConstraints + 1; j++) {
			tableau[row][j] /= pivot;
		}
		// clear pivot column
		for (int i = 0; i < numberOfConstraints + 1; i++) {
			if (i != row) {
				double temp = tableau[i][column];
				for (int j = 0; j < numberOfVariables + numberOfConstraints + 1; j++) {
					tableau[i][j] -= tableau[row][j] * temp;
				}
			}
		}
	}
	
	public double applySimplex() {
		double iteration = 0;
		if (print) {
			print();
		}
		while (true) {
			int column = getColumnIndexOfPivot();
			if (column == -1) {
				return iteration;
			}
			int row = getRowIndexOfPivot(column);
			if (row == -1) {
				System.out.println("Unbounded problem.");
				return iteration;
			}
			pivotOperation(row, column);
			basisIndices[row] = column;
			if (print) {
				print();
			}
			iteration++;
		}
	}
	
	public double getNumberOfIterations() {
		double iterations = 0;
		if (mode == RANDOM) {
			int numberOfTrials = 10000;
			for (int i = 0; i < numberOfTrials; i++) {
				LinearProgram lp = new LinearProgram(coefficientMatrix, constraintVector, costVector, mode, LinearProgram.SUPRESS_OUTPUT);
				iterations += lp.applySimplex();
			}
			iterations /= numberOfTrials;
		} else {
			LinearProgram lp = new LinearProgram(coefficientMatrix, constraintVector, costVector, mode, LinearProgram.SUPRESS_OUTPUT);
			iterations = lp.applySimplex();
		}
		return iterations;
	}
	
	public double[] getPrimalSolution() {
		applySimplex();
		double[] solution = new double[numberOfVariables];
		for (int i = 0; i < numberOfConstraints; i++) {
			if (basisIndices[i] < numberOfVariables) {
				solution[basisIndices[i]] = tableau[i][numberOfVariables + numberOfConstraints];
			}
		}
		return solution;
	}
	
	public double[] getPrimalSlackVariableCost() {
		applySimplex();
		double[] slackVariables = new double[numberOfConstraints];
		for (int i = 0; i < numberOfConstraints; i++) {
			if (basisIndices[i] >= numberOfVariables) {
				slackVariables[basisIndices[i] - numberOfVariables] = tableau[i][numberOfVariables + numberOfConstraints];
			}
		}
		return slackVariables;
	}
	
	public double getObjectiveFunctionValue() {
		applySimplex();
		return - tableau[numberOfConstraints][numberOfVariables + numberOfConstraints];
	}
	
	public double[] getDualSolution() {
		applySimplex();
		double[] dualSolution = new double[numberOfConstraints];
		for (int i = 0; i < numberOfConstraints; i++) {
			dualSolution[i] = - tableau[numberOfConstraints][numberOfVariables + i];
		}
		return dualSolution;
	}
	
	public boolean isPrimalFeasible() {
		double[] primalSolution = getPrimalSolution();
		for (int i = 0; i < numberOfConstraints; i++) {
			double elt = 0;
			for (int j = 0; j < numberOfVariables; j++) {
				elt += coefficientMatrix[i][j] * primalSolution[j];
			}
			if (elt > constraintVector[i] + EPSILON) {
				return false;
			}
		}
		for (int i = 0; i < numberOfVariables; i++) {
			if (primalSolution[i] < 0) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isDualFeasible() {
		double[] dualSolution = getDualSolution();
		for (int i = 0; i < numberOfVariables; i++) {
			double elt = 0;
			for (int j = 0; j < numberOfConstraints; j++) {
				elt += coefficientMatrix[j][i] * dualSolution[j];
			}
			if (elt < costVector[i] - EPSILON) {
				return false;
			}
		}
		for (int i = 0; i < numberOfConstraints; i++) {
			if (dualSolution[i] < 0) {
				return false;
			}
		}
		return true;
	}
	
	public boolean complementarySlacknessHolds() {
		double[] primalSolution = getPrimalSolution();
		double[] dualSolution = getDualSolution();
		// lambda^T (b - Ax) = 0
		double[] bMinusAX = new double[numberOfConstraints];
		for (int i = 0; i < numberOfConstraints; i++) {
			double elt = constraintVector[i];
			for (int j = 0; j < numberOfVariables; j++) {
				elt -= coefficientMatrix[i][j] * primalSolution[j];
			}
			bMinusAX[i] = elt;
		}
		double scalarProductOne = 0;
		for (int i = 0; i < numberOfConstraints; i++) {
			scalarProductOne += dualSolution[i] * bMinusAX[i];
		}
		if (Math.abs(scalarProductOne) > EPSILON) {
			return false;
		}
		// x^T (A^T lambda - c) = 0
		double[] aTransposedLambdaMinusC = new double[numberOfVariables];
		for (int i = 0; i < numberOfVariables; i++) {
			double elt = - costVector[i];
			for (int j = 0; j < numberOfConstraints; j++) {
				elt += coefficientMatrix[j][i] * dualSolution[j];
			}
			aTransposedLambdaMinusC[i] = elt;
		}
		double scalarProductTwo = 0;
		for (int i = 0; i < numberOfVariables; i++) {
			scalarProductTwo += primalSolution[i] * aTransposedLambdaMinusC[i];
		}
		if (Math.abs(scalarProductTwo) > EPSILON) {
			return false;
		}
		return true;
	}

}
